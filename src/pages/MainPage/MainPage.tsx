import React from 'react'
import { EDITOR_TAB, PREVIEW_TAB } from '../../constants'
import Editor from '../../features/Editor'
import Preview from '../../features/Preview'

type Props = {
  currentTab: number
}

const MainPage = ({ currentTab }: Props) => {
  return (
    <>
      {currentTab === EDITOR_TAB && <Editor />}
      {currentTab === PREVIEW_TAB && <Preview />}
    </>
  )
}

export default MainPage
